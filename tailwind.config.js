const plugin = require("tailwindcss/plugin");

module.exports = {
  future: {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: {
    enabled: process.env.HUGO_ENVIRONMENT === "production",
    content: ["./hugo_stats.json"],
    mode: "all",
    options: {
      //whitelist: [ 'pl-1', 'pl-3' ],
      defaultExtractor: (content) => {
        let els = JSON.parse(content).htmlElements;
        els = els.tags.concat(els.classes, els.ids);
        return els;
      },
    },
  },
  theme: {
    typography: (theme) => ({
      default: {
        css: {
          color: theme("colors.gray.900"),
        },
      },
      light: {
        css: {
          color: theme("colors.gray.100"),
          h1: {
            color: theme("colors.gray.100"),
          },
          h2: {
            color: theme("colors.gray.100"),
          },
          h3: {
            color: theme("colors.gray.100"),
          },
          h4: {
            color: theme("colors.gray.100"),
          },
          strong: {
            color: theme("colors.gray.100"),
          },
          a: {
            color: theme("colors.gray.100"),
          },
        },
      },
    }),
    extend: {
      fontFamily: {
        display: ["Bebas Neue", "sans-serif"],
        body: ["Roboto", "sans-serif"],
      },
      colors: {
        primary: "#ac221e",
        primaryLight: "#e45747",
        primaryDark: "#760000",
        secondary: "#d78445",
        secondaryLight: "#ffb472",
        secondaryDark: "#a15718",
      },
    },
  },
  variants: {
    backgroundColor: ({ after }) => after(["even", "odd"]),
    textColor: ({ after }) => after(["even", "odd"]),
  },
  plugins: [
    require("@tailwindcss/typography"),
    plugin(function ({ addVariant, e }) {
      addVariant("inverse", ({ modifySelectors, separator }) => {
        modifySelectors(({ className }) => {
          return `.${e(`inverse${separator}${className}`)}:inverse`;
        });
      });
    }),
  ],
};
