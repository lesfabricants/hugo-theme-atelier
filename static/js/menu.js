(() => {
  const button = document.querySelector("#menu-button");
  if (button) {
    const menu = document.querySelector("#menu-list");
    button.addEventListener("click", () => {
      menu.classList.toggle("hidden");
      button.querySelector("svg path").classList.toggle("hidden");
      button.querySelector("svg path:nth-child(2)").classList.toggle("hidden");
    });
  }
})();
